"""
Contain the methods for parsing builds.
"""

def _parse_tas_build(build):
    """Parse build coming as HTML page. Return ERROR if TAS build page changes 
    and expected tags are not found.
    """
    blist = build.split('\n')
    first_pre_tag_found = False
    second_pre_tag_found = False
    fbuild = ""
    for line in blist:
        if line.find('<pre>') != -1:
            if not first_pre_tag_found:
                first_pre_tag_found = True
                continue
            second_pre_tag_found = True
            fbuild = line.replace('<pre>', '').strip()
        elif second_pre_tag_found:
            fbuild += (" {0}".format(line))
            return fbuild
    return "ERROR"

def _parse_portal_build(build):
    """Parse build. Return ERROR if expected text keys are not found."""
    blist = build.split('\n')
    fbuild = ""
    branch_text = "git.branch: "
    build_text = "git.build.life: "
    for line in blist:
        if line.find(branch_text) == 0:
            fbuild = line[(len(branch_text) - 1):].replace('<br/>', '').strip()
            fbuild += ' '
            continue
        if line.find(build_text) == 0:
            fbuild += line[(len(build_text) - 1):].replace('<br/>', '').strip()
            return fbuild
    return "ERROR"

def _parse_commerceapp_build(build):
    """Parse build. Return ERROR if expected text keys are not found."""
    blist = build.split('\n')
    build_label = "build.label="
    for line in blist:
        if line.find(build_label) == 0:
            return line[len(build_label):].strip()
    return "ERROR"

def parse_build(build, component_name):
    """Parse build for component_name."""
    cn = component_name.lower()
    if cn == "tas":
        return _parse_tas_build(build)
    if cn.find('pa_') == 0: # Portal builds: portlets
        return _parse_portal_build(build)
    if cn == "commerce-app":
        return _parse_commerceapp_build(build)
    return " ".join(build.split('\n')).strip()
