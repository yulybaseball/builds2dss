"""
Retrieve builds for most of PSS-handled systems and create a file with them.
File is then copied to a location defined in conf.json.
"""

import datetime
import json
from os import path
import sys

from . import buildparser as bp
from pssapi.cmdexe import cmd
from pssapi.logger.logger import load_logger
from pssapi.utils import conf

logger = None
EXEC_DIR = path.dirname(path.realpath(sys.argv[0]))
LOG_FILE_NAME = path.join(EXEC_DIR, 'logs', 'builds2dss.log')
CONF_FILE_NAME = path.join(EXEC_DIR, 'conf', 'conf.json')
BUILDS_DIR = path.join(EXEC_DIR, 'logs', 'builds')

def start():
    """Load the logger object and print the initial time to log file."""
    global logger
    logger_fmtter = {
        'INFO': '%(message)s'
    }
    logger = load_logger(log_file_name=LOG_FILE_NAME, cgi=True, **logger_fmtter)
    logger.info('============================================================')
    logger.info(datetime.datetime.now())

def quit_script(error_message=None):
    """Print error_message and datetime into log file. Quit current script."""
    if error_message:
        logger.error(error_message)
    logger.info('Quiting now...')
    logger.info(datetime.datetime.now())
    logger.info('============================================================')
    exit()

def _build_server_name(serv_name, serv_instance):
    """Build and return the complete name of a server from the primary data 
    center, its partial name and instance number.
    TODO: primary data center is defined explicitly in here, but we should 
    take it from the DB once the data access layer has been developed.
    """
    p_data_center = 'dp'
    return "{0}{1}{2}".format(p_data_center, serv_name, serv_instance)

def _command_by_url(server_name, serv_port, url):
    """Build and return the command to be executed if the component build 
    should be retrieved using an URL.
    """
    command = "curl -ks {0}"
    url_data = {'port': serv_port, 'server': server_name}
    complete_url = url.format(**url_data)
    return command.format(complete_url)

def _command_by_path(server_name, user_name, build_path):
    """Build and return the command to be executed if the component build 
    should be retrieved using a path.
    """
    command = "ssh {0}@{1} cat {2}"
    return command.format(user_name, server_name, build_path)

def _get_build(component_name, component_data):
    """Retrieve the build for component_name. Start trying to get the build 
    from the first server for current component, and continue until a build 
    has been successfuly gotten, or until last server has been reached.

    Keyword arguments:
    component_name - Name of the component to get the build from.
    component_data - Dictionary with specific data for this component, 
        like port, url, server name, etc.

    Return the build of the component, or ERROR otherwise.
    """
    first_instance = component_data['first']
    last_instance = component_data['last']
    server_port = component_data['port']
    serv_name = component_data['server']
    url = component_data['url']
    required_keys_not_url = ('build_path', 'user')
    if not url and (not set(required_keys_not_url).issubset(component_data)):
        logger.error("No enough data to process component '{0}'".\
                    format(component_name))
        return "ERROR"
    for server_instance in range(first_instance, (last_instance + 1)):
        server_name = _build_server_name(serv_name, server_instance)
        if url:
            command = _command_by_url(server_name, server_port, url)
        else:
            uname = component_data['user']
            bpath = component_data['build_path']
            command = _command_by_path(server_name, uname, bpath)
        logger.info("\tServer: {0}".format(server_name))
        build = cmd.cmdexec(command)
        if build[1]:
            logger.warning("Error in getting build: {0}".format(build[1]))
        if build[0]:
            return bp.parse_build(build[0], component_name)
    return "ERROR"

def _data2properties(data):
    """Convert dictionary data to a string in the form of a properties file, 
    with the format:
        <key>=<value>
    Return the text containing all key/values in such format.
    """
    txt = ""
    for build in data:
        txt += ("{0}={1}{2}".format(build, data[build], '\n'))
    return txt

def _create_file(file_comp, data, file_name, file_format='json'):
    """Create a file containing the data. Overwrite it if it already exists.
    
    Keyword arguments:
    file_comp - General component to be used as part of the file name 
        (i.e. sf, legacy, web).
    data - Dictionary containing the data to be copied to the new file.
    file_name - Name of the file to create.
    file_format - Format of the content of the file.

    Return the name of the file created.
    """
    current_date = datetime.datetime.now().strftime("%Y%m%d")
    file_name_values = {'component': file_comp, 'date': current_date}
    _file = file_name.format(**file_name_values)
    complete_file_name = path.join(BUILDS_DIR, _file)
    with open(complete_file_name, 'w+') as buildsf:
        if file_format == 'json':
            buildsf.write(json.dumps(data, indent=4))
        else:
            buildsf.write(_data2properties(data))
    return _file

def _copy_files(_files, options, web_usage=False):
    """Copy files coming in dictionary _files to the server:/path coming in 
    dictionary options (defined in conf file).
    If web_usage is True, print the content of the file, so it can be got 
    on a file output when using curl command.
    """
    server = options['server']
    file_path = options['path']
    user = options['user']
    file_name = options['file_name']
    if not file_path:
        logger.warning("File(s) won't be copied: no path has " + 
                      "been defined in configuration file.")
    else:
        logger.info("Copying file(s)...")
        part_command = "scp {0} "
        command = ""
        for _file in _files:
            try:
                complete_file_name = path.join(BUILDS_DIR, _files[_file])
                new_file_name = file_name.format(**{'component': _file.lower()})
                command = part_command.format(complete_file_name)
                if server:
                    command += ("{0}@{1}:".format(user, server))
                new_complete_file_path = path.join(file_path, new_file_name)
                command += new_complete_file_path
                cmd.cmdexec(command.encode())
                if web_usage:
                    logger.warning("'web_usage' has been enabled. Please note " +
                    "this allows the content of the file to be taken " + 
                    "if using a 'curl' command with the option '-o <file>'.")
                    with open(new_complete_file_path, 'r') as f:
                        print(f.read())
                logger.info("File was copied using this command: {0}".format(command))
            except Exception as e:
                logger.error("When executing below command, this error " +
                "happened: \n\tCommand: {0}\n\tError: {1}".format(command, str(e)))

def retrieve_builds(*args):
    """Read from conf file and retrieve the build for every component in there.
    If args have values, just get the builds for the components especified 
    on args. Create the files containing the builds.
    """
    conf_json = conf.get_conf(CONF_FILE_NAME)
    if isinstance(conf_json, str):
        quit_script(conf_json)
    logger.info("Parameters: {0}".format(args))
    logger.info("Retrieving builds...")
    builds = {}
    _files = {}
    builds_file_name = conf_json['builds_file_name']
    file_format = conf_json['destination']['format']
    file_components = [arg.upper() for arg in args[0]] if args[0] \
                                                else conf_json['components']
    for file_component in file_components:
        logger.info("File Component: {0}".format(file_component))
        if file_component not in conf_json['components']:
            logger.error("Parameter {0} is not part of the builds conf file.".\
                        format(file_component))
            continue
        builds[file_component] = {}
        for component in conf_json['components'][file_component]:
            logger.info("\tComponent: {0}".format(component))
            try:
                build = _get_build(component, 
                            conf_json['components'][file_component][component])
                logger.info("\tBuild: {0}".format(build))
            except Exception as e:
                logger.error("Error has occurred: {0}".format(str(e)))
                build = "ERROR"
            finally:
                builds[file_component][component] = build
        logger.info("Creating file for file component: {0}".format(file_component))
        _file = _create_file(file_component.lower(), builds[file_component], 
                             builds_file_name, file_format)
        _files[file_component] = _file
        logger.info("File was created with name: {0}".format(_file))
    _copy_files(_files, conf_json['destination'], conf_json['web_usage'])
    logger.info("Finished retrieving builds.")
