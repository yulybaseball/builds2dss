#!/bin/env python

import sys

def main(*args):
    b2dss.start()
    b2dss.retrieve_builds(*args)
    b2dss.quit_script()

if __name__ == "__main__":
    if __package__ is None:
        # for calling as a script. e.g.
        #   $ ./main
        #   $ python builds2dss/main.py
        from os import path
        import sys
        sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
    # for calling as a package, too. e.g.
    #   $ python -m builds2dss.main
    from builds2dss import b2dss
    main(sys.argv[1:])
